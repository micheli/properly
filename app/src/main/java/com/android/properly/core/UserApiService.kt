package com.android.properly.core

import com.android.properly.data.apiModel.ApiUserCreated
import com.android.properly.data.apiModel.ApiUserDeleted
import com.android.properly.data.apiModel.ApiUsers
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

interface UserApiService {

    companion object {
        private var retrofitService: UserApiService? = null

        fun getInstance(): UserApiService {

            if (retrofitService == null) {
                val retrofit = RetrofitClient.retrofit
                retrofitService = retrofit.create(UserApiService::class.java)
            }
            return retrofitService!!
        }

    }

    @GET("public-api/users")
    fun getUsers(): Call<ApiUsers>

    @POST("public-api/users")
    fun createUser(
        @Body data: JsonObject,
        @Header("Authorization") auth: String
    ): Call<ApiUserCreated>

    @DELETE("public-api/users/{id}")
    fun deleteUser(@Path("id") id: Int, @Header("Authorization") auth: String): Call<ApiUserDeleted>

}