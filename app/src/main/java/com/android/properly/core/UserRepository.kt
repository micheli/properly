package com.android.properly.core

import com.android.properly.utilities.SESSION_TOKEN
import com.google.gson.JsonObject

class UserRepository constructor(private val retrofitService: UserApiService) {

    fun getUsers() = retrofitService.getUsers()

    fun createUser(jo: JsonObject) = retrofitService.createUser(jo, "Bearer $SESSION_TOKEN")

    fun deleteUser(id: Int) = retrofitService.deleteUser(id, "Bearer $SESSION_TOKEN")
}