/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.properly.accountFeature.logFragment.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.properly.databinding.ChangelogItemViewBinding

/**
 * Adapter for the [RecyclerView].
 */
class LogsAdapter :
    RecyclerView.Adapter<LogsAdapter.ChangelogViewHolder>() {
    private var changeLog: List<String> = listOf()

    fun updateList(newList: List<String>) {
        changeLog = newList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = changeLog.size

    /**
     * Creates new views with R.layout.item_view as its template
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChangelogViewHolder {
        val viewBinding = ChangelogItemViewBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ChangelogViewHolder(viewBinding)
    }

    /**
     * Replaces the content of an existing view with new data
     */
    override fun onBindViewHolder(holder: ChangelogViewHolder, position: Int) {
        holder.bind(changeLog[position])
    }


    inner class ChangelogViewHolder(private val viewBinding: ChangelogItemViewBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {


        fun bind(item: String) {
            viewBinding.text.text = item
        }


    }


}