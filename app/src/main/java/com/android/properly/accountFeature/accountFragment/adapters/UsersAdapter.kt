/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.properly.accountFeature.accountFragment.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.properly.data.User
import com.android.properly.databinding.UserItemViewBinding

/**
 * Adapter for the [RecyclerView].
 */
class UsersAdapter :
    RecyclerView.Adapter<UsersAdapter.UsersViewHolder>() {
    private var users: List<User> = listOf()
    var onItemClick: ((User) -> Unit)? = null
    var onItemClickReUpload: ((User) -> Unit)? = null

    fun updateList(newList: List<User>) {
        users = newList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = users.size

    /**
     * Creates new views with R.layout.item_view as its template
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val viewBinding = UserItemViewBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return UsersViewHolder(viewBinding)
    }

    /**
     * Replaces the content of an existing view with new data
     */
    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        holder.bind(users[position])
    }


    inner class UsersViewHolder(private val viewBinding: UserItemViewBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {


        fun bind(item: User) {
            viewBinding.deleteIv
                .setOnClickListener {
                    onItemClick?.invoke(item)
                }
            if (item.userId == 0) {
                viewBinding.container.setBackgroundColor(Color.parseColor("#ADD8E6"))
                viewBinding.container.setOnClickListener {
                    onItemClickReUpload?.invoke(item)
                }
            } else {
                viewBinding.container.setBackgroundColor(Color.parseColor("#FFFFFF"))
            }
            viewBinding.user = item
            viewBinding.executePendingBindings()
        }


    }


}