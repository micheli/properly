package com.android.properly.accountFeature

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.properly.core.UserRepository
import com.android.properly.data.DataInterface
import com.android.properly.data.User
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class AccountViewModel constructor(
    context: Context,
    repository: UserRepository
) : ViewModel() {
    private val dataInterface: DataInterface = DataInterface(context, repository)
    val userList = MutableLiveData<List<User>>()
    private val changeLogList = LinkedList<String>()
    val changeLogStrings = MutableLiveData<List<String>>()
    val errorMessage = MutableLiveData<String>()

    private suspend fun getUsers(orderBy: String? = null) {
        dataInterface.getUsers(orderBy).collect {
            userList.postValue(it)
        }
        changeLogList.add("Retrieved API user")
        changeLogStrings.postValue(changeLogList)
    }

    fun filterEntriesBy(field: String) {
        viewModelScope.launch {
            getUsers(field)
        }
        changeLogList.add("Sorting By $field")
        changeLogStrings.postValue(changeLogList)
    }

    fun deleteUser(user: User) {
        dataInterface.deleteUser(user) {
            viewModelScope.launch {
                getUsers()
            }
        }
        changeLogList.add("Delete user")
        changeLogStrings.postValue(changeLogList)
    }

    fun reUploadUser() {
        viewModelScope.launch {
            dataInterface.reuploadUsers {
                viewModelScope.launch {
                    getUsers()
                }
            }
        }
        changeLogList.add("reUpload User")
        changeLogStrings.postValue(changeLogList)
    }

    fun createUser(user: User) {
        dataInterface.createUser(user) {
            viewModelScope.launch {
                getUsers()
            }
        }
        changeLogList.add("Create User")
        changeLogStrings.postValue(changeLogList)
    }

    fun initialize() {
        dataInterface.initialize {
            viewModelScope.launch {
                getUsers()
            }
        }
        changeLogList.add("Initialize")
        changeLogStrings.postValue(changeLogList)
    }

}