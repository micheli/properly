package com.android.properly.accountFeature.accountFragment

import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.properly.accountFeature.AccountViewModel
import com.android.properly.accountFeature.accountFragment.adapters.UsersAdapter
import com.android.properly.data.User
import com.android.properly.databinding.DialogAccountCreationBinding
import com.android.properly.databinding.FragmentAccountBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class AccountFragment(private val viewModel: AccountViewModel) : Fragment() {
    private var adapter = UsersAdapter()
    private var _binding: FragmentAccountBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAccountBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerList.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerList.adapter = adapter
        adapter.onItemClick = { user ->
            viewModel.deleteUser(user)
        }
        adapter.onItemClickReUpload = {
            viewModel.reUploadUser()
        }
        binding.buttonSortById.setOnClickListener {
            viewModel.filterEntriesBy(
                "id"
            )
        }
        binding.buttonSortByName.setOnClickListener {
            viewModel.filterEntriesBy(
                "name"
            )
        }
        binding.buttonSortByMail.setOnClickListener {
            viewModel.filterEntriesBy(
                "mail"
            )
        }

        viewModel.userList.observe(viewLifecycleOwner, {
            adapter.updateList(it)
        })
        viewModel.errorMessage.observe(viewLifecycleOwner, {
            //manage error
            Toast.makeText(requireActivity(), it, Toast.LENGTH_SHORT).show()
        })
        binding.fab.setOnClickListener {
            DialogAccountCreation.newInstance(viewModel).show(requireActivity().supportFragmentManager, "")
        }
        viewModel.initialize()


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    companion object {
        fun newInstance(viewModel: AccountViewModel) = AccountFragment(viewModel)
    }

    class DialogAccountCreation(private val viewModel: AccountViewModel) : DialogFragment() {
        private var _binding: DialogAccountCreationBinding? = null

        // This property is only valid between onCreateView and
        // onDestroyView.
        private val binding get() = _binding!!

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            _binding = DialogAccountCreationBinding.inflate(inflater, container, false)
            return binding.root

        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            binding.buttonCreateAccount.setOnClickListener {
                //laziness
                if (binding.editTextName.text.toString() != "" && isValidEmail(binding.editTextMail.text.toString())) {
                    val user = User(0, binding.editTextName.text.toString(), binding.editTextMail.text.toString(), "Male", "Offline", "", "")
                    viewModel.createUser(user)
                    dismiss()
                } else {
                    Toast.makeText(
                        requireActivity(),
                        "Please insert valid values...",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        }

        private fun isValidEmail(email: String): Boolean {
            return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        override fun onDestroyView() {
            super.onDestroyView()
            _binding = null
        }

        companion object {
            fun newInstance(viewModel: AccountViewModel) = DialogAccountCreation(viewModel)
        }
    }
}