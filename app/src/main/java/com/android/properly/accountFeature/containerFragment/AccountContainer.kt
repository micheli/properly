package com.android.properly.accountFeature.containerFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.android.properly.accountFeature.AccountViewModel
import com.android.properly.accountFeature.ViewModelFactory
import com.android.properly.accountFeature.accountFragment.AccountFragment
import com.android.properly.accountFeature.logFragment.LogFragment
import com.android.properly.core.UserApiService
import com.android.properly.core.UserRepository
import com.android.properly.databinding.FragmentAccountContainerBinding
import com.google.android.material.tabs.TabLayoutMediator


class AccountContainer : Fragment() {
    private var _binding: FragmentAccountContainerBinding? = null
    private lateinit var adapter: SimpleFragmentPagerAdapter
    private lateinit var viewModel: AccountViewModel
    private val retrofitService = UserApiService.getInstance()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAccountContainerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, ViewModelFactory(requireActivity(), UserRepository(retrofitService))).get(
            AccountViewModel::class.java
        )
        //Setting up ViewPager
        val tabNames = listOf("Account", "Log")
        adapter = SimpleFragmentPagerAdapter(this, viewModel)
        binding.viewPager.adapter = adapter
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = tabNames[position]
        }.attach()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    class SimpleFragmentPagerAdapter(fa: Fragment?, viewModel: AccountViewModel) :
        FragmentStateAdapter(fa!!) {
        private val pageCount = 2
        private val vm = viewModel
        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> AccountFragment.newInstance(vm)
                1 -> LogFragment.newInstance(vm)
                else -> Fragment()
            }
        }

        override fun getItemCount(): Int {
            return pageCount
        }
    }

}