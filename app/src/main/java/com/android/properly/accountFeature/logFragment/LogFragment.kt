package com.android.properly.accountFeature.logFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.properly.accountFeature.AccountViewModel
import com.android.properly.accountFeature.logFragment.adapters.LogsAdapter
import com.android.properly.databinding.FragmentLogBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class LogFragment(private val viewModel: AccountViewModel) : Fragment() {
    private var adapter = LogsAdapter()
    private var _binding: FragmentLogBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentLogBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerList.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerList.adapter = adapter
        viewModel.changeLogStrings.observe(viewLifecycleOwner, {
            adapter.updateList(it)
        })

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        fun newInstance(viewModel: AccountViewModel) = LogFragment(viewModel)
    }

}