package com.android.properly.data.apiModel


import com.google.gson.annotations.SerializedName

data class ApiUserDeleted(
    @SerializedName("code")
    val code: Int = 0,
    @SerializedName("data")
    val `data`: Any? = Any(),
    @SerializedName("meta")
    val meta: Any? = Any()
)