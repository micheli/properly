package com.android.properly.data

import com.android.properly.data.db.DbUser
import com.google.gson.JsonObject

data class User(
    var userId: Int,
    var name: String = "",
    var email: String = "",
    var gender: String = "",
    var status: String = "",
    var created_at: String = "",
    var updated_at: String = "",
) {

    fun toDAO(): DbUser {
        return DbUser(
            userId,
            name,
            email,
            gender,
            status,
            created_at,
            updated_at
        )
    }

    fun toJSON(): JsonObject {
        val rootObject = JsonObject()
        rootObject.addProperty("name", name)
        rootObject.addProperty("email", email)
        rootObject.addProperty("gender", gender)
        rootObject.addProperty("status", "Active")
        return rootObject
    }
}