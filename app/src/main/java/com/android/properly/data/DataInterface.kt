package com.android.properly.data

import android.content.Context
import android.util.Log
import com.android.properly.core.UserRepository
import com.android.properly.data.apiModel.ApiUserCreated
import com.android.properly.data.apiModel.ApiUserDeleted
import com.android.properly.data.apiModel.ApiUsers
import com.android.properly.data.db.AppDatabase
import com.android.properly.data.db.DbUser
import kotlinx.coroutines.flow.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

suspend fun <T> Flow<List<T>>.flattenToList() =
    flatMapConcat { it.asFlow() }.toList()

class DataInterface constructor(
    private val context: Context,
    private val repository: UserRepository
) {

    fun initialize(callback: () -> Unit) {
        val response = repository.getUsers()
        response.enqueue(object : Callback<ApiUsers> {
            override fun onResponse(call: Call<ApiUsers>, response: Response<ApiUsers>) {
                val apiUsers = response.body()
                if (apiUsers != null && apiUsers.code in 200..299) {
                    apiUsers.data.forEach {
                        val dbUser = User(
                            it.id,
                            it.name,
                            it.email,
                            it.gender,
                            it.status,
                            it.createdAt,
                            it.updatedAt
                        ).toDAO()
                        AppDatabase.getInstance(context).userDao().insert(dbUser)
                        callback()
                    }
                }
            }

            override fun onFailure(call: Call<ApiUsers>, t: Throwable) {

            }
        })
    }

    fun getUsers(orderBy: String? = null): Flow<List<User>> {
        val users = when (orderBy) {
            "id" -> AppDatabase.getInstance(context).userDao().getUsersOrderedById()
            "name" -> AppDatabase.getInstance(context).userDao().getUsersOrderedByName()
            "mail" -> AppDatabase.getInstance(context).userDao().getUsersOrderedByMail()
            else -> AppDatabase.getInstance(context).userDao().getUsers()
        }

        return users.map { lst ->
            lst.map {
                User(
                    it.userId,
                    it.name,
                    it.email,
                    it.gender,
                    it.status,
                    it.created_at,
                    it.updated_at
                )
            }
        }
    }

    fun createUser(u: User, callback: () -> Unit) {
        // First, insert in the local DB
        val dbu = u.toDAO()
        AppDatabase.getInstance(context).userDao().insert(dbu)

        // Try to reupload user
        reuploadUserImpl(dbu, callback)
    }

    suspend fun reuploadUsers(callback: () -> Unit) {
        AppDatabase.getInstance(context).userDao().getUserToUpload().collect {
            reuploadUserImpl(it, callback)
        }
    }

    private fun reuploadUserImpl(dbu: DbUser, callback: () -> Unit) {
        if (dbu == null) return
        //Try to push it through the API, if we fail no problem, otherwise update the local DB
        val response = repository.createUser(dbu.toJSON())
        response.enqueue(object : Callback<ApiUserCreated> {
            override fun onResponse(
                call: Call<ApiUserCreated>,
                response: Response<ApiUserCreated>
            ) {
                val apiUserCreated = response.body()
                if (apiUserCreated != null && apiUserCreated.code in 200..299) {
                    dbu.status = "Online"
                    dbu.userId = apiUserCreated.data.id
                    AppDatabase.getInstance(context).userDao().update(dbu)

                    callback()
                } else {
                    // We do not care about errors, we have the local copy!
                    callback()
                }
            }

            override fun onFailure(call: Call<ApiUserCreated>, t: Throwable) {
                Log.d("OnFailure!", "")
                callback()
            }
        })
    }

    fun deleteUser(u: User, callback: () -> Unit) {
        val idToDelete = u.userId
        if (u.status == "Online") {
            val response = repository.deleteUser(idToDelete)
            response.enqueue(object : Callback<ApiUserDeleted> {
                override fun onResponse(
                    call: Call<ApiUserDeleted>,
                    response: Response<ApiUserDeleted>
                ) {
                    val apiUserDeleted = response.body()
                    if (apiUserDeleted != null && apiUserDeleted.code in 200..299) {
                        AppDatabase.getInstance(context).userDao().deleteByUserId(idToDelete)
                    } else {
                        // Could not delete on remote API, book-keep that we will need to delete this sooner or later
                        u.toDAO().status = "Deleted"
                    }
                    callback()
                }

                override fun onFailure(call: Call<ApiUserDeleted>, t: Throwable) {
                    // Could not delete on remote API, book-keep that we will need to delete this sooner or later
                    u.toDAO().status = "Deleted"
                    callback()
                }
            })
        } else {
            AppDatabase.getInstance(context).userDao().deleteByUserId(idToDelete)
        }
    }

    suspend fun tryToDeleteRemoteObjects() {
        AppDatabase.getInstance(context).userDao().getUserToDelete().collect {
            val response = repository.deleteUser(it.userId)
            response.enqueue(object : Callback<ApiUserDeleted> {
                override fun onResponse(
                    call: Call<ApiUserDeleted>,
                    response: Response<ApiUserDeleted>
                ) {
                    val apiUserDeleted = response.body()
                    if (apiUserDeleted != null && apiUserDeleted.code in 200..299) {
                        AppDatabase.getInstance(context).userDao().deleteByUserId(it.userId)
                    }
                }

                override fun onFailure(call: Call<ApiUserDeleted>, t: Throwable) {
                    // Could not delete on remote API, we keep the book-keeping object
                }
            })
        }
    }
}