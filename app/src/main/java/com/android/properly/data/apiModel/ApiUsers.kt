package com.android.properly.data.apiModel


import com.google.gson.annotations.SerializedName

data class ApiUsers(
    @SerializedName("code")
    val code: Int = 0,
    @SerializedName("data")
    val `data`: List<Data> = listOf(),
    @SerializedName("meta")
    val meta: Meta = Meta()
) {
    data class Data(
        @SerializedName("created_at")
        val createdAt: String = "",
        @SerializedName("email")
        val email: String = "",
        @SerializedName("gender")
        val gender: String = "",
        @SerializedName("id")
        val id: Int = 0,
        @SerializedName("name")
        val name: String = "",
        @SerializedName("status")
        val status: String = "",
        @SerializedName("updated_at")
        val updatedAt: String = ""
    )

    data class Meta(
        @SerializedName("pagination")
        val pagination: Pagination = Pagination()
    ) {
        data class Pagination(
            @SerializedName("limit")
            val limit: Int = 0,
            @SerializedName("page")
            val page: Int = 0,
            @SerializedName("pages")
            val pages: Int = 0,
            @SerializedName("total")
            val total: Int = 0
        )
    }
}