package com.android.properly.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.JsonObject

@Entity(tableName = "users")
data class DbUser(
    @ColumnInfo(name = "id") var userId: Int,
    @ColumnInfo(name = "name") var name: String = "",
    @PrimaryKey @ColumnInfo(name = "email") var email: String = "",
    @ColumnInfo(name = "gender") var gender: String = "",
    @ColumnInfo(name = "status") var status: String = "", // TODO: Use status to mark users tht are Online, Offline or Deleted
    @ColumnInfo(name = "created_at") var created_at: String = "",
    @ColumnInfo(name = "updated_at") var updated_at: String = "",
) {

    fun toJSON(): JsonObject {
        val rootObject = JsonObject()
        rootObject.addProperty("name", name)
        rootObject.addProperty("email", email)
        rootObject.addProperty("gender", gender)
        rootObject.addProperty("status", "Active")
        return rootObject
    }
}