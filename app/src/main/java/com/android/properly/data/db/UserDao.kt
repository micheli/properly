package com.android.properly.data.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

/**
 * The Data Access Object for the Plant class.
 */
@Dao
interface UserDao {
    @Query("SELECT * FROM users WHERE status != 'Deleted' ORDER BY name")
    fun getUsers(): Flow<List<DbUser>>

    @Query("DELETE FROM users WHERE id = :userId")
    fun deleteByUserId(userId: Int)

    @Query("SELECT * FROM users WHERE status != 'Deleted' ORDER BY id")
    fun getUsersOrderedById(): Flow<List<DbUser>>

    @Query("SELECT * FROM users WHERE status != 'Deleted' ORDER BY name")
    fun getUsersOrderedByName(): Flow<List<DbUser>>

    @Query("SELECT * FROM users WHERE status != 'Deleted' ORDER BY email")
    fun getUsersOrderedByMail(): Flow<List<DbUser>>

    @Query("SELECT * FROM users WHERE id = :id")
    fun getUser(id: String): Flow<DbUser>

    @Query("SELECT * FROM users WHERE id = :id")
    fun getUserToReUpload(id :Int): Flow<DbUser>

    @Query("SELECT * FROM users WHERE status == 'Deleted'")
    fun getUserToDelete(): Flow<DbUser>

    @Query("SELECT * FROM users WHERE status == 'Offline'")
    fun getUserToUpload(): Flow<DbUser>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: DbUser)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(users: List<DbUser>)

    @Update
    fun update(user: DbUser)
}
