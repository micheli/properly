package com.android.properly.data.apiModel


import com.google.gson.annotations.SerializedName

data class ApiUserCreated(
    @SerializedName("code")
    val code: Int = 0,
    @SerializedName("data")
    val `data`: Data = Data(),
    @SerializedName("meta")
    val meta: Any? = Any()
) {
    data class Data(
        @SerializedName("created_at")
        val createdAt: String = "",
        @SerializedName("email")
        val email: String = "",
        @SerializedName("gender")
        val gender: String = "",
        @SerializedName("id")
        val id: Int = 0,
        @SerializedName("name")
        val name: String = "",
        @SerializedName("status")
        val status: String = "",
        @SerializedName("updated_at")
        val updatedAt: String = ""
    )
}